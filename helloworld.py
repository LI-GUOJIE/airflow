from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta

def print_hello():
    print('Hello World!')

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 5, 5),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

with DAG('example_hello_world_dag',
         default_args=default_args,
         schedule_interval=timedelta(days=1)) as dag:

    date_task = BashOperator(
        task_id='date_task',
        bash_command='date'
    )

    hello_task = PythonOperator(
        task_id='hello_task',
        python_callable=print_hello
    )

    sleep_task = BashOperator(
        task_id='sleep_task',
        bash_command='sleep 5'
    )

    date_task >> hello_task >> sleep_task